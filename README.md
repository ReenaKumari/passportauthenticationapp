# README #

This is a authentication application using mean stack and passport node-modules.
Using passport we can hash the password authenticate , accepts string arguments , hashed it, and compare it with the current user's hashed password.

### Tools Required ###
     *Git Bash
     *Node.js
     *MongoDB
     *RoboMongo
     *WebStorm IDE
     *postman

### Node Modules Used ###
     *express
    *body-parser
    *chalk
    *consolidate
    *glob
    *lodash
    *mongoose
    *swig (Template Engine)
    *Passprt
    *Passport-local







